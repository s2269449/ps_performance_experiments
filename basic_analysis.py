import sys 
import numpy as np
import matplotlib.pyplot as plt 

    
filename = sys.argv[1]

total_updates = 0
iterations = 0
data = []
# First, open out an Output file
with open(filename, 'r') as f:
    for line in f.readlines():
        if 'Number of' in line:
            iterations += 1
            line = line.replace('\n','')
            line = line.split(' ')
            updates = line[7] 
            total_updates += int(updates)
            data.append((iterations, int(updates)))

average_updates = total_updates / iterations


print()
print("###################################")
print("Performance Data")
print("###################################")
print()
print('Amount of iterations')
print(iterations)
print()
print('Total amount of squares updated')
print(total_updates)
print()
print('Average squares updates per iteration')
average_updates = total_updates / iterations 
print(average_updates)
print("###################################")
print()

# Produce the data to plot iterations against sqaures updated
xs = [x[0] for x in data]
ys = [x[1] for x in data]


plt.plot(xs, ys, marker=".", markersize=3) 
plt.xlabel("Iterations")
plt.ylabel("Updated sqaures")
plt.show()

plotfilename = ('plot_of_' + filename + '.png')
plt.savefig(plotfilename)

