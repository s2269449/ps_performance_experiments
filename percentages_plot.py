import sys 
import numpy as np
import matplotlib.pyplot as plt 

    
filename = sys.argv[1]

total_updates = 0
iterations = 0
data = []


with open(filename, 'r') as f:
    for line in f.readlines():
        if 'Number of' in line:
            iterations += 1
            line = line.replace('\n','')
            line = line.split(' ')
            updates = line[7] 
            total_updates += int(updates)
            data.append((iterations, int(updates)))

average_updates = total_updates / iterations

points = [x for x in range(0, 101, 5)]
for x in range(len(points)): 
    points[x] /= 100 

curr_it = 0
curr_up = 0
up_sum = 0
relative_data = []
curr_threshold = 0.1
thresholds = []
for point in data:
    curr_it = point[0]
    curr_up = point[1]
    up_sum += curr_up
    rel_up = up_sum / total_updates
    rel_it = curr_it / iterations
    relative_data.append((rel_it, rel_up))
    if rel_up > curr_threshold: 
        print("Completed", rel_up*100, "% of total updates after", rel_it*100, "& of total iterations")
        curr_threshold += 0.1
    
    
# Data plot for relative updates and iterations
its = [x[0] for x in relative_data]
ups = [x[1] for x in relative_data]
plt.plot(its, ups, marker=".", markersize=1, label="Rel. Updates Completed" )
plt.plot(its, its, marker=".", markersize=1, label="Rel. Iterations Completed")
plt.xlabel("Percent of iterations completed")
plt.ylabel("Percent of updates completed")
plt.legend(loc="upper left")
#plt.show()
plt.savefig("relative_plot.png")
