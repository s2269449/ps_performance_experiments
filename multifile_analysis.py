import sys 
import numpy as np
import matplotlib.pyplot as plt 

files = []
for filename in sys.argv[1:]:
    files.append(filename)    

print(files)

total_updates = 0
iterations = 0
data = []
averages = []
iterations = []
# First, open out an Output file
for f in files:
    curr_data = []
    curr_total_updates = 0
    curr_iterations = 0
    with open(f, 'r') as f:
        for line in f.readlines():
            if 'Number of' in line:
                curr_iterations += 1
                line = line.replace('\n','')
                line = line.split(' ')
                updates = line[7] 
                curr_total_updates += int(updates)
                curr_data.append((curr_iterations, int(updates)))
    data.append(curr_data)
    averages.append(curr_total_updates / curr_iterations)
    iterations.append(curr_iterations)


for count, point in enumerate(data):
    xs = [x[0] for x in point]
    ys = [x[1] for x in point]
    plt.plot(xs, ys, label=("Grid size of " + str(sys.argv[count+1])[:-4]))
    xs = []
    ys = []

plt.xlabel("Iterations")
plt.ylabel("Updated squares")
plt.legend(loc="upper right")

#plt.show()
#plt.savefig('multiplot.png') 
